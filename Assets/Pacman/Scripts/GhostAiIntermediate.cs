﻿using UnityEngine;
using System.Collections;

public class GhostAiIntermediate : MonoBehaviour {

	public float speed;
	private Vector2 destination;
	private Vector2 direction;
	private Animator chAnimator;

	void Awake ()
	{
		destination = Vector2.zero;
		direction = Vector2.zero;
		chAnimator = GetComponent<Animator> ();
	}

	void Start ()
	{
		speed = GameController.Instance.ghostSpeed;
		direction = Vector2.right;
		destination = transform.position;
	}

	void FixedUpdate ()
	{
		Vector2 p = Vector2.MoveTowards (transform.position, destination, speed);
		GetComponent<Rigidbody2D> ().MovePosition (p);

		if (Vector2.Distance (destination, transform.position) < 0.00001f)
		{
			Vector2 tempDirection = Vector2.zero;
			Vector2 tempPacmanPosition = PacmanController.Instance.transform.position;

			if (tempPacmanPosition.x - transform.position.x > 0.25f ||
			    tempPacmanPosition.x - transform.position.x < -0.25f)
			{
				if (tempPacmanPosition.x > transform.position.x &&
				    Valid (Vector2.right))
				{
					tempDirection = Vector2.right;
				}
				else if (tempPacmanPosition.x < transform.position.x &&
				         Valid (Vector2.left))
				{
					tempDirection = Vector2.left;
				}
				else
				{
					if (tempPacmanPosition.y > transform.position.y)
					{
						if (Valid (Vector2.up))
							tempDirection = Vector2.up;
						else if (Valid (Vector2.down))
							tempDirection = Vector2.down;
						else
							tempDirection = Vector2.zero;
					}
					else if (tempPacmanPosition.y < transform.position.y)
					{
						if (Valid (Vector2.down))
							tempDirection = Vector2.down;
						else if (Valid (Vector2.up))
							tempDirection = Vector2.up;
						else
							tempDirection = Vector2.zero;
					}
				}
			}
			else if (tempPacmanPosition.y - transform.position.y > 0.25f ||
			         tempPacmanPosition.y - transform.position.y < -0.25f)
			{
				if (tempPacmanPosition.y > transform.position.y &&
				    Valid (Vector2.up))
				{
					tempDirection = Vector2.up;
				}
				else if (tempPacmanPosition.y < transform.position.y &&
				         Valid (Vector2.down))
				{
					tempDirection = Vector2.down;
				}
				else
				{
					if (tempPacmanPosition.x > transform.position.x)
					{
						if (Valid (Vector2.right))
							tempDirection = Vector2.right;
						else if (Valid (Vector2.left))
							tempDirection = Vector2.left;
						else
							tempDirection = Vector2.zero;
					}
					else if (tempPacmanPosition.x < transform.position.x)
					{
						if (Valid (Vector2.left))
							tempDirection = Vector2.left;
						else if (Valid (Vector2.right))
							tempDirection = Vector2.right;
						else
							tempDirection = Vector2.zero;
					}
				}
			}

/*
			if (Valid (Vector2.right) && tempPacmanPosition.x > transform.position.x)
				tempDirection = Vector2.right;
			else if (Valid (Vector2.up) && tempPacmanPosition.y > transform.position.y)
				tempDirection = Vector2.up;
			else if (Valid (Vector2.down) && tempPacmanPosition.y < transform.position.y)
				tempDirection = Vector2.down;
			else if (Valid (Vector2.left) && tempPacmanPosition.x < transform.position.x)
				tempDirection = Vector2.left;
			else
			{
				if (Valid (direction))
					tempDirection = Vector2.zero;
			}
*/
			destination = (Vector2)transform.position + tempDirection;
			direction = tempDirection;
			chAnimator.SetFloat ("DirX", direction.x);
			chAnimator.SetFloat ("DirY", direction.y);
		}
	}

	bool Valid (Vector2 direction)
	{
		Vector2 pos = transform.position;
		direction += new Vector2 (direction.x * 0.45f, direction.y * 0.45f);
		RaycastHit2D hit = Physics2D.Linecast (pos + direction, pos);

		if (hit.collider == null)
		{
			return true;
		}
		else
		{
			if (hit.collider.transform == this.transform ||
			    hit.transform.CompareTag ("Player") ||
			    hit.transform.CompareTag ("PacDot") ||
			    hit.transform.CompareTag ("PacEnergizer"))
				return true;
			else
				return false;
		}
	}
}
