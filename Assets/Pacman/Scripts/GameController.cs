﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	public static GameController Instance;

	public float pacmanSpeed;
	public float ghostSpeed;

	void Awake ()
	{
		if (!Instance)
			Instance = this;
		else
			Destroy (this.gameObject);
	}
}
